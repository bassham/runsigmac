import os
import sys
import yaml
import io
from ruamel.yaml import YAML
from pathlib import Path
from ruamel.yaml.compat import StringIO

# Modules to "choose dir"
import tkinter as tk
from tkinter import filedialog

def loadYaml(filename):
    with open(filename, 'r') as stream:
        try:
            print(yaml.safe_load(stream))
        except yaml.YAMLError as exc:
            print(exc)

def main():
    print("Please select directory location of Sigmac...")
    root = tk.Tk()
    dirPath = filedialog.askdirectory()
    # Walk dir of selected sigmac until rules directory is reached
    rulesPath = os.walk(dirPath + '/rules')
    root.withdraw()
    rootDir = os.path.abspath(dirPath)  # Changed selected dir to an 'absolute path'
    sigmacDir = os.chdir(rootDir + "/tools")
    #confFile = "'" + rootDir + "/tools/config/generic/sysmon.yml" + "'" # config file
    confFile = "'" + rootDir + "/tools/config/elk-winlogbeat.yml" + "'"  # config file
    rulesDir = "'" + rootDir + "/rules/windows/" + "'"
    yamlFile = rootDir + "/tools/YAML.yml"

    ruleTemplate = """
    title: "test_title"
    description: "test_description"
    detection:
        query: "test"
    """
    for root, subdirs, files in rulesPath:
        for subdir in subdirs:
            if "1=1":
                pass
        for filename in files:
            file_path = os.path.join(root, filename)
            fileConvert = "'" + file_path + "'"
            rootDir = os.path.abspath(dirPath)
            # cd so that sigmac can be launched
            sigmacDir = os.chdir(rootDir + "/tools")
            sigmacDir
            # run Sigmac
            cmd = "./sigmac -I -t es-qs -c {0} {1}".format(confFile, fileConvert)
            runSigmac = os.popen(cmd).read()
            # Load ruleTemplate and add converted rule to 'query' section
            yaml = YAML()
            data = yaml.load(ruleTemplate)
            data['detection']['query'] = runSigmac
            # Direct output to yaml file
            sys.stdout = open(yamlFile, 'a')
            yaml.dump(data, sys.stdout)


    #loadYaml(yamlFile) # verify yaml file loads

if __name__ == "__main__":
    main()
