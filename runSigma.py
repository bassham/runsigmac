import os
import sys
import yaml

# Modules to "choose dir"
import tkinter as tk
from tkinter import filedialog

def parse_sigma_rules(rule):
    with open(rule, 'r') as stream:
        try:
            parsed_config = yaml.safe_load(stream)
            print(parsed_config)
        except yaml.YAMLError as exc:
            print(exc)

def main():
    # variables
    # tkinter
    print("Please select directory location of Sigmac...")
    root = tk.Tk()
    dirPath = filedialog.askdirectory()
    # Walk dir of selected sigmac until rules directory is reached
    rulesPath = os.walk(dirPath + '/rules')
    root.withdraw()
    rootDir = os.path.abspath(dirPath)  # Changed selected dir to an 'absolute path'
    sigmacDir = os.chdir(rootDir + "/tools")
    confFile = "'" + rootDir + "/tools/config/generic/sysmon.yml" + "'" # config file
    rulesDir = "'" + rootDir + "/rules/windows/" + "'"
    outFile = rootDir + "/tools/esOutput.txt"
    sigmacDir # cd to sigmac location
    cmd = "./sigmac -I -t es-qs -c {0} -r {1} -o {2}".format(confFile,rulesDir,outFile)
    os.system(cmd)

    #parse_sigma_rules(outFile)

    # stream = open(outFile, "r")
    # docs = yaml.load(stream, Loader=yaml.FullLoader)
    # for doc in docs:
    #     for tag1,tag2 in doc.items():
    #         print(tag1, "->", tag2)
    #     print("\n",)

    # THIS IS WHAT BEN TRIED...
    ruleTemplate = """
    title: {0}
    description: {1}
    detection:
        query: {2}
    """
    with open(outFile, 'r') as f:
        lines = f.readlines()
        for line in lines:
            print(ruleTemplate.format("test_title", "test_description", line))

if __name__ == "__main__":
    main()
