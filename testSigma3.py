import os
import sys
import yaml
import argparse
import os.path

OUTPUT_RULE_TEMPLATE = """
  title: {0}
  description: {1}
  detection:
    query: {2}
  """

def run_sigmac(sigmaExe, confFile, sigmaRuleFile):
    print("sigma exe: {}\nconfig file: {}\nrule file: {}\n\n\n".format(sigmaExe, confFile, sigmaRuleFile))
    cmd = "{0} -I -t es-qs -c {1} {2}".format(sigmaExe, confFile, sigmaRuleFile)
    res = os.popen(cmd).read()
    return res

def get_rule_info(sigmaRuleFile):
    with open(sigmaRuleFile, 'r') as stream:
        try:
            parsed_config = yaml.safe_load(stream)
            return {"title": parsed_config['title'], "description": parsed_config['description']}
        except yaml.YAMLError as exc:
            print(exc)

def pull_updates():
    return False

def main():
    parser = argparse.ArgumentParser(description='Parse SIGMA rules and put them into data miner output')
    parser.add_argument('-s', help='Sigma exe', required=True)
    parser.add_argument('-d', help='Sigma rule directory', required=True)
    parser.add_argument('-c', help='Sigma config file', required=True)
    parser.add_argument('-u', help='Pull Updates')
    args = parser.parse_args()

    updated = False
    if args.u is True:
        updated = pull_updates()

    user_input = "Y"
    if updated:
        user_input = raw_input("Updates found, do you want to continue? [Y/n]")


    if user_input is "Y" or user_input is "y":
        for root, dirs, files in os.walk(args.d):
            for file in files:
                if "apt" not in os.path.join(root, file):
                    print('Filename: {}/{}'.format(root, file))
                if '.yml' in file:
                    try:
                        cwd = os.getcwd()
                        query = run_sigmac(args.s, args.c, os.path.join(root, file))
                        info = get_rule_info(os.path.join(root, file))
                        outPath = "/output"
                        outPath = os.path.join(root,outPath)
                        joinPath = os.path.join(cwd, r'outPath')
                        if not os.path.exists(joinPath):
                            print (joinPath)
                            os.makedirs(joinPath)
                        path = "{1}/{0}.yaml".format(info['title'],joinPath)
                        if not os.path.isdir(path):
                            print("**************************************************************************\n")
                            print("** Converting {0} into Net-Alert YAML Format **\n".format(path))
                            print("**************************************************************************\n")
                        with open(path.format(info['title']), 'w') as ruleOutput:
                            try:
                                if os.path.isdir(path):
                                    print("Path exist...")
                            except Exception as e:
                                print(e)
                            ruleOutput.write(OUTPUT_RULE_TEMPLATE.format(info['title'], info['description'], query))
                    except Exception as exec:
                        print(exec)
                        pass
                else:
                    print("No YAML file found")

if __name__ == "__main__":
    main()
