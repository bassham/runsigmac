import os
import sys
import yaml
import io

# Modules to "choose dir"
import tkinter as tk
from tkinter import filedialog

def parse_sigma_rules(rule):
    with open(rule, 'r') as stream:
        try:
            parsed_config = yaml.safe_load(stream)
            print(parsed_config)
        except yaml.YAMLError as exc:
            print(exc)

def main():
    # variables
    # tkinter
    print("Please select directory location of Sigmac...")
    root = tk.Tk()
    dirPath = filedialog.askdirectory()
    # Walk dir of selected sigmac until rules directory is reached
    rulesPath = os.walk(dirPath + '/rules')
    root.withdraw()
    rootDir = os.path.abspath(dirPath)  # Changed selected dir to an 'absolute path'
    sigmacDir = os.chdir(rootDir + "/tools")
    #confFile = "'" + rootDir + "/tools/config/generic/sysmon.yml" + "'" # config file
    confFile = "'" + rootDir + "/tools/config/elk-winlogbeat.yml" + "'" # config file
    rulesDir = "'" + rootDir + "/rules/windows/" + "'"
    outFile = rootDir + "/tools/esOutput.txt"
    outYaml = rootDir + "/tools/yamlOut.yml"



    #cmd = "./sigmac -I -t es-qs -c {0} -r {1} -o {2}".format(confFile,rulesDir,outFile)
    #os.system(cmd)

    #additional vars


    for root, subdirs, files in rulesPath:


        # Create variable for output file
        #list_file_path = os.path.join(root, 'test-my-directory-list.txt')
        # sub dir level
        for subdir in subdirs:
            print('\t- subdirectory ' + subdir)
    # file level
        for filename in files:
            file_path = os.path.join(root, filename)
            fileConvert = "'" + file_path + "'"
            print ("file convert = {0}".format(fileConvert))

            rootDir = os.path.abspath(dirPath)
            print (rootDir)
            # cd so that sigmac can be launched
            sigmacDir = os.chdir(rootDir + "/tools")
            sigmacDir

            cwd = os.getcwd()
            print(cwd)

            cmd = "./sigmac -I -t es-qs -c {0} {1}".format(confFile,fileConvert)
            runSigmac = os.system(cmd)
            runSigmac

            ruleTemplate = """
            title: {0}
            description: {1}
            detection:
                query: {2}
            """
            with open(outFile, 'r') as f:
                lines = f.readlines()
                for line in lines:
                    #print(yaml.dump(ruleTemplate).format("test_title", "test_description", line))
                    myFile = open(outYaml, 'a')
                    #var1 = print(ruleTemplate.format("test_title", "test_description", line))
                    myFile.write("%s" % ruleTemplate.format("test_title", "test_description", line))
                myFile.close()

    # Load yaml
    #yaml.safe_load(outYaml)
    with open(outYaml, 'r') as stream:
            try:
                parsed_config = yaml.safe_load(stream)
                print(parsed_config)
            except yaml.YAMLError as exc:
                print(exc)


    #with open(outYaml, 'r') as ymlfile:
     #   cfg = yaml.safe_load(ymlfile)






            #print(ruleTemplate.format("test_title", "test_description", line))



    #os.remove(outFile)

    #parse_sigma_rules(outFile)

    # stream = open(outFile, "r")
    # docs = yaml.load(stream, Loader=yaml.FullLoader)
    # for doc in docs:
    #     for tag1,tag2 in doc.items():
    #         print(tag1, "->", tag2)
    #     print("\n",)

    # THIS IS WHAT BEN TRIED...

    #stream = open(outFile, "r")


'''
    with open(outFile, 'r') as f:
        lines = f.readlines()
        for line in lines:
            print(ruleTemplate.format("test_title", "test_description", line))
'''
if __name__ == "__main__":
    main()
